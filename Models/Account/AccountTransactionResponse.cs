﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class AccountTransactionResponse
    {
        public int AccountNumber { get; set; }
        public bool Successful { get; set; }
        public decimal Balance { get; set; }
        public string Currency { get; set; }
        public string Message { get; set; }
        public string TransactionNo { get; set; }
    }
}
