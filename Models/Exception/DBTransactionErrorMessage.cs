﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ExceptionHandling
{
    public class DBTransactionErrorMessage
    {

        public static string DbInsertionFailedError
        {
            get
            {
                return GetExceptionMessage(DbTransactionExceptionCode.DbInsertionFailedError, " :Error Occured while creating the record");
            }
        }

        /// <summary>
        /// Method to return the errror message for DB Updation failure
        /// </summary>
        public static string DbUpdationFailedError
        {
            get
            {
                return GetExceptionMessage(DbTransactionExceptionCode.DbUpdationFailedError, " :Error occured while updating the record");
            }
        }

        /// <summary>
        /// Method to return the errror message allong with the error code for application id null
        /// </summary>
        public static string DbTransactionError
        {
            get
            {
                return GetExceptionMessage(DbTransactionExceptionCode.DbTransactionError, " :Error Occured while transaction and rolled back");
            }
        }

        /// <summary>
        /// Method to return the errror message allong with the error code for db record exists 
        /// </summary>
        public static string DbRecordExists
        {
            get
            {
                return GetExceptionMessage(DbTransactionExceptionCode.DbRecordExists, " :Error Occured while insertion as record already exists with same value");
            }
        }

        /// <summary>
        /// Method to return the errror message allong with the error code for db delettion failed
        /// </summary>
        public static string DbDeletionFailedError
        {
            get
            {
                return GetExceptionMessage(DbTransactionExceptionCode.DbDeletionFailedError, " :Error Occured while deleting the records");
            }
        }

        /// <summary>
        /// Method to return the errror message allong with the error code for db delettion failed
        /// </summary>
        public static string DbQueryException
        {
            get
            {
                return GetExceptionMessage(DbTransactionExceptionCode.DbQueryException, " :Error Occured while fetching the records");
            }
        }



        private static string GetExceptionMessage(string ExceptionCode, string Message)
        {
            return Convert.ToInt64(ExceptionCode) + " . " + Message;
        }

    }
}
