﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
    public class AccountTransaction
    {
        public int AccountID { get; set; }
        public decimal Amount { get; set; }        
        public TransactionType TransactionType { get; set; }
        public Currency Currency { get; set; }
    }
}
