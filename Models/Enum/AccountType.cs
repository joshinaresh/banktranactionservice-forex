﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Enum
{
    public enum AccountType
    {
        Unknown = 1,
        Savings = 2,
        Current = 3
    }
}
