﻿using Models;
using Models.Constants;
using Models.ExceptionHandling;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;

namespace BankTransactionConsoleClient
{
    public class SimulateTransactions
    {
        public SimulateTransactions(int totalAccountTobeCreated, int totalTransactionToBePermormedOnEachAccount, string baseUri)
        {

            List<Account> accountList = new List<Account>();
            for (int i = 0; i < totalAccountTobeCreated; i++)
            {

                Account account = new Account()
                {
                    FirstName = $"FirstName_{(new Random().Next(1000, 40000))}",
                    LastName = $"LastName_{(new Random().Next(1000, 40000))}",
                    DOB = DateTime.Now.AddYears(-(new Random().Next(10, 25))),
                    AccountType = Models.Enum.AccountType.Savings,
                    UserName = $"UserName_{i}_{(new Random().Next(10, 2500))}",
                    Password = "changeme",
                    CurrentBalance = 0,
                    AccountCurrencyID = (Currency)(new Random().Next(1, 6)),
                    LocalCurrencyID = Currency.INR
                };

                using (HttpClient accountCreateClient = new HttpClient())
                {
                    accountCreateClient.BaseAddress = new Uri(baseUri);
                    string accountData = JsonConvert.SerializeObject(account);
                    var contentData = new StringContent(accountData, System.Text.Encoding.UTF8, "application/json");
                    Stopwatch accountWatch = new Stopwatch();
                    accountWatch.Start();
                    HttpResponseMessage accountCreateResponse = accountCreateClient.PostAsync(requestUri: "/api/Account/CreateAccount", content: contentData).Result;
                    accountWatch.Stop();

                    switch (accountCreateResponse.StatusCode)
                    {
                        case System.Net.HttpStatusCode.Created:
                            var currentAccount = JsonConvert.DeserializeObject<AccountTransactionResponse>(accountCreateResponse.Content.ReadAsStringAsync().Result);
                            account.AccountId = currentAccount.AccountNumber;
                            Console.WriteLine("-----------------------------------------------------------");
                            Console.WriteLine(" ");
                            Console.WriteLine($"Account for Customer : { account.FirstName} created successfully, with AccountID : { account.AccountId } and Default Currency as {account.AccountCurrencyID}");
                            Console.WriteLine(" ");
                            Console.WriteLine("-----------------------------------------------------------");
                            accountList.Add(account);
                            break;
                        case System.Net.HttpStatusCode.BadRequest:
                            Console.WriteLine("-----------------------------------------------------------");
                            Console.WriteLine("Account details provided are not as expected format");
                            Console.WriteLine("-----------------------------------------------------------");
                            break;
                        default:
                            Console.WriteLine("-----------------------------------------------------------");
                            Console.WriteLine("Something went wrong");
                            Console.WriteLine("-----------------------------------------------------------");
                            break;

                    }
                }
            }

            foreach (var account in accountList)
            {

                for (int j = 0; j < totalTransactionToBePermormedOnEachAccount; j++)
                {
                    var transactionType = ((new Random().Next(0, 1000)) % 2) == 0 ? TransactionType.Credit : TransactionType.Debit;

                    switch (transactionType)
                    {
                        case TransactionType.Credit:
                            using (HttpClient accountDepositClient = new HttpClient())
                            {
                                AccountTransaction objdeposit = new AccountTransaction()
                                {
                                    AccountID = account.AccountId,
                                    Amount = new Random().Next(200, 10000),
                                    Currency = account.AccountCurrencyID,
                                    TransactionType = TransactionType.Credit
                                };
                                accountDepositClient.BaseAddress = new Uri(baseUri);
                                string depositData = JsonConvert.SerializeObject(objdeposit);
                                var contentData = new StringContent(depositData, System.Text.Encoding.UTF8, "application/json");
                                Stopwatch depositWatch = new Stopwatch();
                                depositWatch.Start();
                                HttpResponseMessage depositresponse = accountDepositClient.PostAsync(requestUri: "/api/Account/Deposit", content: contentData).Result;
                                depositWatch.Stop();

                                switch (depositresponse.StatusCode)
                                {
                                    case System.Net.HttpStatusCode.OK:
                                        AccountTransactionResponse depositAccountTransactionResponse = JsonConvert.DeserializeObject<AccountTransactionResponse>(depositresponse.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine($"Credit transaction Report for AccountID: { account.AccountId } and TransactionNo. : { depositAccountTransactionResponse.TransactionNo } ");
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine($"Response Message : { depositAccountTransactionResponse.Message}");
                                        Console.WriteLine($"Account Balance (upon transaction) : { depositAccountTransactionResponse.Balance}");
                                        Console.WriteLine($"Current Transaction Amount : { objdeposit.Amount}");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        break;
                                    case System.Net.HttpStatusCode.BadRequest:
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine("Account Transaction details provided are not as expected format");
                                        Console.WriteLine("-----------------------------------------------------------");
                                        break;
                                    default:
                                        BankServiceError depositBankServiceError = JsonConvert.DeserializeObject<BankServiceError>(depositresponse.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine("Something went wrong");
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine($"Response Message : { depositBankServiceError.Message}");
                                        break;
                                }
                            }
                            break;
                        case TransactionType.Debit:
                            using (HttpClient accountWithdrawClient = new HttpClient())
                            {
                                AccountTransaction objWithdraw = new AccountTransaction()
                                {
                                    AccountID = account.AccountId,
                                    Amount = new Random().Next(5000, 15000),
                                    Currency = account.AccountCurrencyID,
                                    TransactionType = TransactionType.Credit
                                };
                                accountWithdrawClient.BaseAddress = new Uri(baseUri);
                                string withdrawData = JsonConvert.SerializeObject(objWithdraw);
                                var withdrawContenData = new StringContent(withdrawData, System.Text.Encoding.UTF8, "application/json");
                                Stopwatch withdrawWatch = new Stopwatch();
                                withdrawWatch.Start();
                                HttpResponseMessage withdrawResponse = accountWithdrawClient.PostAsync(requestUri: "/api/Account/Withdraw", content: withdrawContenData).Result;
                                withdrawWatch.Stop();

                                switch (withdrawResponse.StatusCode)
                                {
                                    case System.Net.HttpStatusCode.OK:
                                        AccountTransactionResponse withdrawAccountTransactionResponse = JsonConvert.DeserializeObject<AccountTransactionResponse>(withdrawResponse.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine($"Debit transaction Report for AccountID : { account.AccountId } and TransactionNo. : { withdrawAccountTransactionResponse.TransactionNo }");
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine($"Response Message : { withdrawAccountTransactionResponse.Message}");
                                        Console.WriteLine($"Account Balance (upon transaction) : { withdrawAccountTransactionResponse.Balance}");
                                        Console.WriteLine($"Current Transaction Amount : { objWithdraw.Amount}");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        break;
                                    case System.Net.HttpStatusCode.BadRequest:
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine("Account Transaction details provided are not as expected format");
                                        Console.WriteLine("-----------------------------------------------------------");
                                        break;
                                    default:
                                        BankServiceError withdrawBankServiceError = JsonConvert.DeserializeObject<BankServiceError>(withdrawResponse.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine("Something went wrong");
                                        Console.WriteLine("-----------------------------------------------------------");
                                        Console.WriteLine($"Response Message : { withdrawBankServiceError.Message}");
                                        break;
                                }
                            }
                            break;
                    }
                }
            }
        }
    }
}


