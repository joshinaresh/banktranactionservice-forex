﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Constants
{
    public static class ValidationErrorMessages
    {
        public const string AccountDetailsSuccess = "Account details fetched successfully";
        public const string AccountDetailsFailure = "Failed to fetch the account details";

        public const string AccountCreateSuccees = "New Account created successfully";
        public const string AccountCreateFailure = "Failed to create New Account";


        public const string AccountUpdateSuccees = "Account updated successfully";
        public const string AccountUpdateFailure = "Failed to updated the Account";

        public const string CurrencyNotSupported = "Provided Currency currently not supported by System";
        public const string InsufficientFunds = "Withdraw Transaction Unsuccessfull, due to insufficient balance";
    }
}
