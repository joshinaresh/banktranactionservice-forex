using BankTransactionService.Controllers;
using BusinessAccessContract;
using DataAccess.ConnectionFactory;
using DataAccessContract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Moq;
using System.Threading.Tasks;

namespace BankTransactionService.Test
{
    [TestClass]
    public class UnitTest1
    {
        Mock<ITransactionManager> mockTransactionManager = new Mock<ITransactionManager>();


        [TestMethod]
        public async Task CreateAccountSuccess()
        {

            Account accountToCreate = new Account()
            {
                FirstName = "Johns",
                LastName = "Jimmy"
            };

            AccountTransactionResponse accountTransactionResponse = new AccountTransactionResponse()
            {
                AccountNumber = 1,
                Balance = 20
            };

            mockTransactionManager.Setup(x => x.CreateAccountAsync(accountToCreate)).ReturnsAsync(accountTransactionResponse);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.CreateAccount(accountToCreate);

            Assert.AreEqual(StatusCodes.Status201Created, ((ObjectResult)result).StatusCode);
        }

        [TestMethod]
        public async Task CreateAccountFailure()
        {

            Account accountToCreate = null;



            mockTransactionManager.Setup(x => x.CreateAccountAsync(accountToCreate)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);

            var result = await obj.CreateAccount(accountToCreate);
            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);
        }

        [TestMethod]
        public async Task BalanceSuccess()
        {

            int accountID = 5;

            AccountTransactionResponse accountTransactionResponse = new AccountTransactionResponse()
            {
                AccountNumber = 5,
                Balance = 20
            };

            mockTransactionManager.Setup(x => x.GetAccountBalanceAsync(accountID)).ReturnsAsync(accountTransactionResponse);

            AccountController obj = new AccountController(mockTransactionManager.Object);

            var result = await obj.Balance(accountID);
            Assert.AreEqual(StatusCodes.Status200OK, ((ObjectResult)result).StatusCode);
        }


        [TestMethod]
        public async Task BalanceFailure()
        {
            int accountID = 0;

            mockTransactionManager.Setup(x => x.GetAccountBalanceAsync(accountID)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);

            var result = await obj.Balance(accountID);
            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);
        }


        [TestMethod]
        public async Task DepositSuccess()
        {

            AccountTransaction accountTransaction = new AccountTransaction()
            {
                AccountID = 1,
                Amount = 100,
                Currency = Currency.INR
            };

            AccountTransactionResponse accountTransactionResponse = new AccountTransactionResponse()
            {
                AccountNumber = 1,
                Balance = 100
            };


            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(accountTransactionResponse);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Deposit(accountTransaction);

            Assert.AreEqual(StatusCodes.Status200OK, ((ObjectResult)result).StatusCode);
            Assert.AreEqual(accountTransaction.AccountID, ((AccountTransactionResponse)((ObjectResult)result).Value).AccountNumber);
        }

        [TestMethod]
        public async Task DepositFailure()
        {

            AccountTransaction accountTransaction = null;

           
            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Deposit(accountTransaction);

            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);
           
        }

        [TestMethod]
        public async Task DepositFailureForAccountIDZero()
        {

            AccountTransaction accountTransaction = new AccountTransaction()
            {
                AccountID = 0
            };


            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Deposit(accountTransaction);

            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);

        }

        [TestMethod]
        public async Task DepositFailureForTransactionAmountZero()
        {

            AccountTransaction accountTransaction = new AccountTransaction()
            {
                AccountID = 1,
                Amount = 0
            };


            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Deposit(accountTransaction);

            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);

        }

        [TestMethod]
        public async Task WithdrawSuccess()
        {

            AccountTransaction accountTransaction = new AccountTransaction()
            {
                AccountID = 1,
                Amount = 100,
                Currency = Currency.INR
            };

            AccountTransactionResponse accountTransactionResponse = new AccountTransactionResponse()
            {
                AccountNumber = 1,
                Balance = 1000
            };


            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(accountTransactionResponse);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Withdraw(accountTransaction);

            Assert.AreEqual(StatusCodes.Status200OK, ((ObjectResult)result).StatusCode);
            Assert.AreEqual(accountTransaction.AccountID, ((AccountTransactionResponse)((ObjectResult)result).Value).AccountNumber);
        }

        [TestMethod]
        public async Task WithdrawFailure()
        {

            AccountTransaction accountTransaction = null;

            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Withdraw(accountTransaction);

            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);

        }

        [TestMethod]
        public async Task WithdrawFailureForAccountIDZero()
        {

            AccountTransaction accountTransaction = new AccountTransaction()
            {
                AccountID = 0
            };


            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Withdraw(accountTransaction);

            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);

        }

        [TestMethod]
        public async Task WithdrawFailureForTransactionAmountZero()
        {

            AccountTransaction accountTransaction = new AccountTransaction()
            {
                AccountID = 1,
                Amount = 0
            };


            mockTransactionManager.Setup(x => x.UpdateAccountBalanceAsync(accountTransaction)).ReturnsAsync(It.IsAny<AccountTransactionResponse>);

            AccountController obj = new AccountController(mockTransactionManager.Object);
            var result = await obj.Withdraw(accountTransaction);

            Assert.AreEqual(StatusCodes.Status400BadRequest, ((StatusCodeResult)result).StatusCode);

        }

    }
}
