﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Models.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankTransactionService.Filter
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
    public class GlobalExceptionHandler : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            BankServiceError bankServiceError = null;

            if (context.Exception is BankTransactionException)
            {

                var customException = context.Exception as BankTransactionException;

                bankServiceError = new BankServiceError(customException.Message, customException.StatusCode)
                {
                    Errors = customException.Errors
                };

                context.Exception = null;
            }
            else if (context.Exception.InnerException is BankTransactionException)
            {

                var customException = context.Exception.InnerException as BankTransactionException;

                bankServiceError = new BankServiceError(customException.Message, customException.StatusCode)
                {
                    Errors = customException.Errors
                };

                context.Exception = null;

            }
            else
            {
                // Unhandled errors

                var browserMessage = "Please Contact Admin!!!, We have encountered a delay/downtime in Response";

                bankServiceError  = new BankServiceError(browserMessage)
                {
                    Detail = "",
                    StatusCode = "500"
                };

            }

            //// always return a JSON result
            context.Result = new ObjectResult(bankServiceError) { StatusCode = StatusCodes.Status500InternalServerError };
            context.ExceptionHandled = true;
            base.OnException(context);
        }
    }
}
