﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Common
{
    public class ConfigurationSetting
    {
        public string TransactionDBConnectionString { get; set; }
        public string APIForexKey { get; set; }
    }
}
