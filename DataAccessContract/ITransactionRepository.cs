﻿using Models;
using System;
using System.Threading.Tasks;

namespace DataAccessContract
{
    public interface ITransactionRepository
    {
        Task<Account> CreateAccountAsync(Account account);

        Task<Account> UpdateAccountDetailsAsync(Account account);

        Task<Account> GetAccountBalanceAsync(int accountID);

        Task<Account> UpdateAccountBalanceAsync(AccountTransaction accountTransaction, decimal currentBalanceLocal);

        Task<bool> IsAccountBalanceValid(int accountID);

        Task<string> AddAccountAuditHistory(AccountAuditHistory accountAuditHistory);

        Task<string> UpdateAccountAuditHistory(AccountAuditHistory accountAuditHistory);
    }
}
