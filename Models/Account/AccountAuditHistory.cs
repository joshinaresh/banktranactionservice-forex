﻿using Models.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class AccountAuditHistory
    {
        public int AccountID { get; set; }
        public decimal CurrentTransactionalAmountAmount { get; set; }
        public decimal CurrentBalance { get; set; }
        public TransactionType TransactionTypeID { get; set; }
        public string AccountDefaultCurrency { get; set; }
        public string CurrentTransactionCurrency { get; set; }
        public TransactionStatus Status { get; set; }
        public string TransactionNo { get; set; }
    }
}
