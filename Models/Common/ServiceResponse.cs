﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class ServiceResponse
    {
        public int AccountID { get; set; }
        public bool Successful { get; set; }
        public decimal Balance { get; set; }
        public Currency Currency { get; set; }
    }
}
