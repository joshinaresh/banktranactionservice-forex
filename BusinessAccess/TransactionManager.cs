﻿using BusinessAccessContract;
using System;
using Models;
using System.Threading.Tasks;
using DataAccessContract;
using SharedUtilty;
using Models.Constants;
using Models.Enum;

namespace BusinessAccess
{
    public class TransactionManager : ITransactionManager
    {
        // Dependency definition
        private readonly ITransactionRepository _transactionRepository;
        private readonly ICurrencyConverter _currencyConverter;

        // Inject the dependencies
        public TransactionManager(ITransactionRepository transactionRepository, ICurrencyConverter currencyConverter)
        {
            this._transactionRepository = transactionRepository;
            this._currencyConverter = currencyConverter;
        }


        /// <summary>
        /// Create Account 
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<AccountTransactionResponse> CreateAccountAsync(Account account)
        {
            var result = await ResponseMapper(await _transactionRepository.CreateAccountAsync(account));
            result.Message = result.Successful ? ValidationErrorMessages.AccountCreateSuccees : ValidationErrorMessages.AccountCreateFailure;
            return result;
        }

        /// <summary>
        /// GetAccountBalance
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public async Task<AccountTransactionResponse> GetAccountBalanceAsync(int accountID)
        {
            var result = await ResponseMapper(await _transactionRepository.GetAccountBalanceAsync(accountID));
            result.Message = result.Successful ? ValidationErrorMessages.AccountDetailsSuccess : ValidationErrorMessages.AccountDetailsFailure;
            return result;
        }

        /// <summary>
        /// UpdateAccount
        /// </summary>
        /// <param name="accountTransaction"></param>
        /// <returns></returns>
        public async Task<AccountTransactionResponse> UpdateAccountBalanceAsync(AccountTransaction accountTransaction)
        {
            var result = new AccountTransactionResponse() { Successful = false };
            decimal transactionalBalance = 0;
            decimal transactionalBalanceLocal = 0;
            string transactionNo = string.Empty;

            // Check if the currency is supported by the system
            if (accountTransaction.Currency == 0)
            {
                result.Message = ValidationErrorMessages.CurrencyNotSupported;
                return result;
            }

            // fetch the account details for the account provided for current transaction
            var account = await _transactionRepository.GetAccountBalanceAsync(accountTransaction.AccountID);

            // check if the accounts exits
            if (account == null)
            {
                result.Message = ValidationErrorMessages.AccountDetailsFailure;
                return result;
            }

            // checks if the Account default currency is same as current transaction currency
            if (accountTransaction.Currency != account.AccountCurrencyID)
            { 
                // convert the current transaction amount with respect current currency
                transactionalBalance = await _currencyConverter.Convert(((Currency)account.AccountCurrencyID).ToString(), ((Currency)accountTransaction.Currency).ToString(), accountTransaction.Amount);
            }
            else
            {
                transactionalBalance = accountTransaction.Amount;
            }
            // create the audit log
            transactionNo = await LogAccountHistory(account, accountTransaction, transactionalBalance, TransactionStatus.Started, string.Empty);
            

            // check if the credit or debit
            if (accountTransaction.TransactionType == TransactionType.Debit)
            {
                if ((account.CurrentBalance - transactionalBalance) > 0)
                {
                    accountTransaction.Amount = (account.CurrentBalance - transactionalBalance);
                   
                }
                else
                {
                    await LogAccountHistory(account, accountTransaction, transactionalBalance, TransactionStatus.Failed, transactionNo);
                    result.TransactionNo = transactionNo;
                    result.Message = ValidationErrorMessages.InsufficientFunds;
                    return result;
                }
            }
            else
            {
                accountTransaction.Amount = (account.CurrentBalance + transactionalBalance);
                
            }

            // Convert the transaction amount in local currency where the application is been deployed
            transactionalBalanceLocal = await _currencyConverter.Convert(((Currency)GlobalConstants.DefaultCurrencyForApplication).ToString(), ((Currency)account.AccountCurrencyID).ToString(), accountTransaction.Amount);

            result = await ResponseMapper(await _transactionRepository.UpdateAccountBalanceAsync(accountTransaction, transactionalBalanceLocal));


            // check if the update is successful
            if (result.Successful)
            {
                await LogAccountHistory(account, accountTransaction, transactionalBalance, TransactionStatus.Success, transactionNo);
                result.TransactionNo = transactionNo;
                result.Message = ValidationErrorMessages.AccountUpdateSuccees;
            }
            else
            {
                await LogAccountHistory(account, accountTransaction, transactionalBalance, TransactionStatus.Failed, transactionNo);
                result.TransactionNo = transactionNo;
                result.Message = ValidationErrorMessages.AccountUpdateFailure;
            }
            return result;
        }


        /// <summary>
        /// UpdateAccountDetailsAsync : Update the Account Details
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<AccountTransactionResponse> UpdateAccountDetailsAsync(int accountID, Account account)
        {

            var result = new AccountTransactionResponse() { Successful = false };
            var accountDetails = await _transactionRepository.GetAccountBalanceAsync(accountID);

            if (accountDetails == null)
            {
                result.Message = ValidationErrorMessages.AccountDetailsFailure;
                return result;
            }

            result = await ResponseMapper(await _transactionRepository.UpdateAccountDetailsAsync(account));
            result.Message = result.Successful ? ValidationErrorMessages.AccountUpdateFailure : ValidationErrorMessages.AccountUpdateSuccees;
            return result;
        }


        /// <summary>
        /// ResponseMapper : Mapp the Account Model to the AccountTransactionResposne
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        private async Task<AccountTransactionResponse> ResponseMapper(Account account)
        {
            if (account != null && account.AccountId > 0)
            {
                return new AccountTransactionResponse()
                {
                    AccountNumber = account.AccountId,
                    Balance = account.CurrentBalance,
                    Currency = ((Currency)account.AccountCurrencyID).ToString(),
                    Successful = true
                };
            }
            return new AccountTransactionResponse() { Successful = false };
        }


        /// <summary>
        /// Log the transaction for audit purpose
        /// </summary>
        /// <param name="account"></param>
        /// <param name="accountTransaction"></param>
        /// <returns></returns>
        public async Task<string> LogAccountHistory(Account account, AccountTransaction accountTransaction, decimal currentTransactionAmount, TransactionStatus status, string TransactionNo)
        {
            //fabricate the AccountAuditHistory model
            var accountAuditHistory = new AccountAuditHistory()
            {
                AccountID = account.AccountId,
                CurrentTransactionalAmountAmount = currentTransactionAmount,
                AccountDefaultCurrency = ((Currency)account.AccountCurrencyID).ToString(),
                CurrentTransactionCurrency = ((Currency)accountTransaction.Currency).ToString(),
                TransactionTypeID = accountTransaction.TransactionType,
                Status = status
            };

            // check if the transaction is started or failure/success
            if (status == TransactionStatus.Started)
            {
                return await _transactionRepository.AddAccountAuditHistory(accountAuditHistory);
            }
            else
            {
                accountAuditHistory.CurrentBalance = account.CurrentBalance;
                accountAuditHistory.TransactionNo = TransactionNo;
                return await _transactionRepository.UpdateAccountAuditHistory(accountAuditHistory);
            }
        }
    }
}
