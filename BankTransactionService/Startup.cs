﻿using BankTransactionService.Filter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Models.Common;
using SimpleInjector;
using SimpleInjector.Integration.AspNetCore.Mvc;
using SimpleInjector.Lifestyles;

namespace BankTransactionService
{
    public class Startup
    {
        protected Container Container;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Container = new Container();
        }

        

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(new GlobalExceptionHandler());
            });

            services.AddSwaggerGen(c =>
             {
                 c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "BANK API", Version = "1" });
             });

            IntegrateSimpleInjector(services);

            ConfigurationSetting configurationSetting = new ConfigurationSetting()
            {
                TransactionDBConnectionString = Configuration.GetSection("ConfigurationSettings")["TransactionDBConnectionString"],
                APIForexKey = Configuration.GetSection("ConfigurationSettings")["APIForexKey"]
            };

            Container.RegisterSingleton<ConfigurationSetting>(() => configurationSetting);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BANK API V1");
            });

            RegisterServices();
        }


        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            //AsyncScopedLifestyle : There will be only one instance of a given service type within a certain (explicitly defined) scope.
            //This scope will automatically flow with the logical flow of control of asynchronous methods
            Container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            services.AddSingleton<IControllerActivator>(new SimpleInjectorControllerActivator(Container));
            services.EnableSimpleInjectorCrossWiring(Container);
            services.UseSimpleInjectorAspNetRequestScoping(Container);
        }

        public void RegisterServices()
        {
            DependencyResolver.DependencyResolver.RegisterServices(Container);
        }
    }
}
