﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Enum
{
    public enum TransactionStatus
    {
        Unknown = 0,
        Started = 1,
        Pending = 2,
        Success = 3,
        Failed = 4
    }
}
