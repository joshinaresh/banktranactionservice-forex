** Bank Application Service - using realtime forex exchange

Bank Application Service consist of API’s that processes the transaction like Balance, Deposit and Withdraw over the respective Account with multi-currency support.
Below is the quick snapshot of the Account API:

The Application allows to the create the new Account and later using Deposit and Withdraw endpoints by providing the AccountID,  Amount,  TransactionType (debit or credit) and the type of Currency for current transaction.
	Sample Transaction Payload:-
	{
	  "AccountID": Int ,
	  "Amount": decimal,
	  "TransactionType": Int,
	  "Currency": Int
	}  

	Transaction Type:
	Credit = 1,
	Debit = 2,
	BalanceCheck = 3

	Currency:
	INR = 1,
	USD = 2,
	THB = 3,
	AUD = 4,
	GBP = 5,
	CHF = 6
