﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Constants
{
    public static class DbTransactionExceptionCode
    {
        /// <summary>
        /// Exception Code to hold when the DB exception while performing transaction
        /// </summary>
        public const string DbTransactionError = "5001";

        /// <summary>
        /// Exception Code to hold when the DB insertion failed while storing 
        /// </summary>
        public const string DbInsertionFailedError = "5002";

        /// <summary>
        /// Exception Code to hold when the DB insertion failed while storing 
        /// </summary>
        public const string DbUpdationFailedError = "5003";

        /// <summary>
        /// Db record exists
        /// </summary>
        public const string DbRecordExists = "5004";

        /// <summary>
        /// Exception Code to hold when the DB deletion failed while storing 
        /// </summary>
        public const string DbDeletionFailedError = "5005";

        /// <summary>
        /// Exception Code to hold when the DB deletion failed while storing 
        /// </summary>
        public const string DbQueryException = "5006";
    }
}
