﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Models.ExceptionHandling
{
    public class BankServiceError
    {
        public BankServiceError()
        { }

        public BankServiceError(string message)
        { }

        public BankServiceError(string message, string statusCode)
        {
            Message = message;
            StatusCode = statusCode;
        }

        public string StatusCode { get; set; }

        public string Message { get; set; }

        public Exception Errors { get; set; }

        public string Detail { get; set; }
    }
}
