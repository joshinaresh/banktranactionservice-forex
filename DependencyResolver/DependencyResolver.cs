﻿using BusinessAccess;
using BusinessAccessContract;
using DataAccess;
using DataAccess.ConnectionFactory;
using DataAccessContract;
using SharedUtilty;
using SimpleInjector;
using System;

namespace DependencyResolver
{
    public static class DependencyResolver
    {
        public static void RegisterServices(Container container)
        {
            container.Register<ITransactionRepository, TransactionRespository>();
            container.Register<IConnectionFactory, ConnectionFactory>();
            container.Register<ITransactionManager, TransactionManager>();
            container.Register<ICurrencyConverter, CurrencyConverter>();
        }
    }
}
