﻿using DataAccessContract;
using System;
using Models;
using System.Threading.Tasks;
using System.Data;
using DataAccess.ConnectionFactory;
using Dapper;
using Models.Constants;
using Models.ExceptionHandling;

namespace DataAccess
{
    public class TransactionRespository : ITransactionRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        public TransactionRespository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        /// <summary>
        /// AddAccountAuditHistory
        /// </summary>
        /// <param name="accountAuditHistory"></param>
        /// <returns></returns>
        public async Task<string> AddAccountAuditHistory(AccountAuditHistory accountAuditHistory)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();

                    var param = new
                    {
                        AccountID = accountAuditHistory.AccountID,
                        CurrentTransactionalAmount = accountAuditHistory.CurrentTransactionalAmountAmount,
                        TransactionTypeId = accountAuditHistory.TransactionTypeID,
                        AccountDefaultCurrency = accountAuditHistory.AccountDefaultCurrency,
                        CurrentTransactionCurrency = accountAuditHistory.CurrentTransactionCurrency,
                        Status = accountAuditHistory.Status
                    };

                    return await connection.QueryFirstOrDefaultAsync<string>("LogAccountAuditHistory", (object)param, null, null, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbInsertionFailedError) { StatusCode = DbTransactionExceptionCode.DbInsertionFailedError, Errors = ex.InnerException };
            }
        }

        /// <summary>
        /// CreateAccountAsync
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<Account> CreateAccountAsync(Account account)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();

                    var param = new
                    {
                        FirstName = account.FirstName,
                        MiddleName = account.MiddleName,
                        LastName = account.LastName,
                        DOB = account.DOB,
                        AccountTypeId = account.AccountType,
                        UserName = account.UserName,
                        Password = account.Password,
                        Balance = account.CurrentBalance,
                        DefaultCurrencyID = (Currency)GlobalConstants.DefaultCurrencyForApplication,
                        AccountCurrencyID = account.AccountCurrencyID
                    };

                    return await connection.QueryFirstOrDefaultAsync<Account>("CreateAccount", (object)param, null, null, CommandType.StoredProcedure);
                };
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbInsertionFailedError) { StatusCode = DbTransactionExceptionCode.DbInsertionFailedError, Errors = ex.InnerException };
            }
        }

        /// <summary>
        /// GetAccountBalanceAsync
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public async Task<Account> GetAccountBalanceAsync(int accountID)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();
                    var param = new
                    {
                        AccountID = accountID
                    };
                    return await connection.QueryFirstOrDefaultAsync<Account>("GetAccountBalance", (object)param, null, null, CommandType.StoredProcedure);
                };
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbQueryException) { StatusCode = DbTransactionExceptionCode.DbQueryException, Errors = ex.InnerException };
            }
        }

        /// <summary>
        /// IsAccountBalanceValid
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public async Task<bool> IsAccountBalanceValid(int accountID)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();
                    var param = new
                    {
                        AccountID = accountID
                    };
                    return await connection.QueryFirstOrDefaultAsync<bool>("GetIsAccountBalanceValid", param, null, null, CommandType.StoredProcedure);
                };
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbQueryException) { StatusCode = DbTransactionExceptionCode.DbQueryException, Errors = ex.InnerException };
            }
        }

        /// <summary>
        /// UpdateAccountAuditHistory
        /// </summary>
        /// <param name="accountAuditHistory"></param>
        /// <returns></returns>
        public async Task<string> UpdateAccountAuditHistory(AccountAuditHistory accountAuditHistory)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();

                    var param = new
                    {
                        AccountID = accountAuditHistory.AccountID,
                        CurrentTransactionalAmount = accountAuditHistory.CurrentTransactionalAmountAmount,
                        CurrentBalance = accountAuditHistory.CurrentBalance,
                        Status = accountAuditHistory.Status,
                        TransactionNo = accountAuditHistory.TransactionNo
                    };

                    return await connection.QueryFirstOrDefaultAsync<string>("UpdateLogAccountAuditHistory", (object)param, null, null, CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbUpdationFailedError) { StatusCode = DbTransactionExceptionCode.DbUpdationFailedError, Errors = ex.InnerException };
            }
        }

        /// <summary>
        /// UpdateAccountBalanceAsync
        /// </summary>
        /// <param name="accountTransaction"></param>
        /// <param name="currentBalanceLocal"></param>
        /// <returns></returns>
        public async Task<Account> UpdateAccountBalanceAsync(AccountTransaction accountTransaction, decimal currentBalanceLocal)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();
                    var param = new
                    {
                        AccountID = accountTransaction.AccountID,
                        CurrentBalance = accountTransaction.Amount,
                        CurrentBalanceLocal = currentBalanceLocal
                    };
                    return await connection.QueryFirstOrDefaultAsync<Account>("UpdateAccountBalance", param, null, null, CommandType.StoredProcedure);
                };
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbUpdationFailedError) { StatusCode = DbTransactionExceptionCode.DbUpdationFailedError, Errors = ex.InnerException }; ;
            }
        }

        /// <summary>
        /// UpdateAccountDetailsAsync
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<Account> UpdateAccountDetailsAsync(Account account)
        {
            try
            {
                using (IDbConnection connection = await _connectionFactory.CreateConnectionAsync())
                {
                    connection.Open();
                    var param = new
                    {
                        AccountID = account.AccountId,
                        FirstName = account.FirstName,
                        Lastname = account.LastName,
                        MiddleName = account.MiddleName
                    };
                    return await connection.QueryFirstOrDefaultAsync<Account>("UpdateAccountDetails", param, null, null, CommandType.StoredProcedure);
                };
            }
            catch (Exception ex)
            {
                throw new BankTransactionException(DBTransactionErrorMessage.DbUpdationFailedError) { StatusCode = DbTransactionExceptionCode.DbUpdationFailedError, Errors = ex.InnerException };
            }
        }
    }
}
