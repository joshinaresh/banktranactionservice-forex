﻿using Models.Enum;
using System;
using System.ComponentModel.DataAnnotations;


namespace Models
{
    public class Account
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public AccountType AccountType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime AccountCreated { get; set; }
        public decimal CurrentBalance { get; set; }
        public Currency LocalCurrencyID { get; set; }
        public Currency AccountCurrencyID { get; set; }
    }
}

