﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.ExceptionHandling
{
    public class BankTransactionException : Exception
    {
        /// <summary>
        /// Status code of the application exception
        /// </summary>
        public string StatusCode { get; set; }

        /// <summary>
        /// Exceptions
        /// </summary>
        public Exception Errors { get; set; }

        /// <summary>
        /// Error Message to be logged 
        /// </summary>
        public override string Message { get; }

        /// <summary>
        /// StackTrace to be logged
        /// </summary>
        public override string StackTrace { get; }

        /// <summary>
        /// Exception data if any to be logged
        /// </summary>
        public override IDictionary Data { get; }

        /// <summary>
        /// Exception specific data to be logged
        /// </summary>
        public object ExceptionData { get; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public BankTransactionException()
        { }

        /// <summary>
        /// Constructor with error message
        /// </summary>
        /// <param name="message"></param>
        public BankTransactionException(string message)
            : base(message)
        {
            Message = message;
        }

        /// <summary>
        /// constructor with error message and code
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public BankTransactionException(string code, string message)
        {
            StatusCode = code;
            Message = message;
        }
        
        /// <summary>
        /// Constructor with the code, message and exception
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public BankTransactionException(string code, string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
