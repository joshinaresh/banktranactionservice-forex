﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public enum TransactionType
    {
        Unknown = 0,
        Credit = 1,
        Debit = 2,
        BalanceCheck = 3
    }
}
