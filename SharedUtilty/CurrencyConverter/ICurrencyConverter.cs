﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharedUtilty
{
    public interface ICurrencyConverter
    {
        Task<decimal> Convert(string from, string to, decimal amount);
    }
}
