﻿using Models;
using Models.Enum;
using System;
using System.Threading.Tasks;

namespace BusinessAccessContract
{
    public interface ITransactionManager
    {
        Task<AccountTransactionResponse> CreateAccountAsync(Account account);

        Task<AccountTransactionResponse> UpdateAccountDetailsAsync(int accountID, Account account);

        Task<AccountTransactionResponse> GetAccountBalanceAsync(int accountID);

        Task<AccountTransactionResponse> UpdateAccountBalanceAsync(AccountTransaction account);

        Task<string> LogAccountHistory(Account account, AccountTransaction accountTransaction, decimal currentTransactionAmount, TransactionStatus status, string TransactionNo);
    }
}
