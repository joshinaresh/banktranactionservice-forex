﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BusinessAccessContract;
using Models;
using BankTransactionService.Filter;

namespace BankTransactionService.Controllers
{
    [Produces("application/json")]
    [Route("api/[Controller]")]

    public class AccountController : Controller
    {

        private readonly ITransactionManager _transactionManager;

        public AccountController(ITransactionManager transactionManager)
        {
            _transactionManager = transactionManager;
        }


        /// <summary>
        /// Balance : TO fetch the balance for provided accountID
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Balance/{accountID}")]
        public async Task<IActionResult> Balance(int accountID)
        {
            if (accountID > 0)
            {
                return StatusCode(StatusCodes.Status200OK, await _transactionManager.GetAccountBalanceAsync(accountID));
            }
            return StatusCode(StatusCodes.Status400BadRequest);
        }

        /// <summary>
        /// Deposit : To Deposit in the respective account
        /// 
        /// Sample Json :
        /// {
        ///     "accountID": 1, -- AccountID of the AccountHolder
        ///     "amount": 10, -- Anount to be Deposited
        ///     "transactionType": 1, -- Transaction Type :- 1 = Credit 
        ///     "currency": 2 -- Type of currency :- 1 = INR, 2 = USD, 3 = THB
        /// }
        ///  
        /// </summary>
        /// <param name="accountTransaction"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Deposit")]
        public async Task<IActionResult> Deposit([FromBody] AccountTransaction accountTransaction)
        {
            if (accountTransaction != null && accountTransaction.AccountID > 0 && accountTransaction.Amount > 0)
            {
                accountTransaction.TransactionType = TransactionType.Credit;
                return StatusCode(StatusCodes.Status200OK, await _transactionManager.UpdateAccountBalanceAsync(accountTransaction));
            }
            return StatusCode(StatusCodes.Status400BadRequest);

        }

        /// <summary>
        /// Withdraw : To Withdraw in the respective account
        /// 
        /// Sample Json :
        /// {
        ///     "accountID": 1, -- AccountID of the AccountHolder
        ///     "amount": 10, -- Anount to be Withdraw
        ///     "transactionType": 1, -- Transaction Type :- 2 = Debit 
        ///     "currency": 2 -- Type of currency :- 1 = INR, 2 = USD, 3 = THB
        /// }
        /// 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountTransaction"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Withdraw")]
        public async Task<IActionResult> Withdraw([FromBody] AccountTransaction accountTransaction)
        {
            if (accountTransaction != null && accountTransaction.AccountID > 0 && accountTransaction.Amount > 0)
            {
                accountTransaction.TransactionType = TransactionType.Debit;
                return StatusCode(StatusCodes.Status200OK, await _transactionManager.UpdateAccountBalanceAsync(accountTransaction));
            }
            return StatusCode(StatusCodes.Status400BadRequest);

        }


        /// <summary>
        /// Create Account : Create Account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateAccount")]
        public async Task<IActionResult> CreateAccount([FromBody] Account account)
        {
            if (account != null)
            {
                return StatusCode(StatusCodes.Status201Created, await _transactionManager.CreateAccountAsync(account));
            }
            return StatusCode(StatusCodes.Status400BadRequest);
        }

        /// <summary>
        /// Update Account Details like FirstName (Todo : Add more dynamism to the update details)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateAccount")]
        public async Task<IActionResult> UpdateAccountDetails(int id, [FromBody] Account account)
        {
            if (account != null)
            {
                return StatusCode(StatusCodes.Status200OK, await _transactionManager.UpdateAccountDetailsAsync(id, account));
            }
            return StatusCode(StatusCodes.Status400BadRequest);
        }

    }
}