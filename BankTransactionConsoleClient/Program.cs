﻿using System;

namespace BankTransactionConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            int totalNumberOfAccountsTobeCreated = 5;
            int totalTransactionToBePermormedOnEachAccount = 2;
            string bankServiceBaseURI = "http://localhost:50415/";

            // perform automatic acount creation and transactions
            SimulateTransactions simulateAccountCreatetion = new SimulateTransactions(totalNumberOfAccountsTobeCreated, totalTransactionToBePermormedOnEachAccount, bankServiceBaseURI);
            Console.ReadLine();
        }
    }
}
