﻿using Models;
using Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.ConnectionFactory
{
    public class ConnectionFactory : IConnectionFactory
    {

        private readonly string _connectionString;
        public ConnectionFactory(ConfigurationSetting configurationSetting)
        {
            _connectionString = configurationSetting.TransactionDBConnectionString;
        }

        public async Task<IDbConnection> CreateConnectionAsync()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
 