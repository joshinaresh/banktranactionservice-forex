﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public enum AccountPermissionType
    {
        Unknown = 1,
        MultiCurrentTransferAllowed = 2,
        MultiCurrentTransferNotAllowed = 3
    }
}
