﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Authorize
{
    public class AccountLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
