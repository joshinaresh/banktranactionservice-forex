﻿using Api.Forex.Sharp;
using Models.Common;
using System;
using System.Threading.Tasks;

namespace SharedUtilty
{
    public class CurrencyConverter : ICurrencyConverter
    {
        private readonly string APIKey = string.Empty;
        public CurrencyConverter(ConfigurationSetting configurationSetting)
        {
            APIKey = configurationSetting.APIForexKey;
        }

        public async Task<decimal> Convert(string from, string to, decimal amount)
        {
            var ForexRates = await ApiForex.GetRate(APIKey);
            return ForexRates.Convert(from, to, amount);
        }
    }
}
