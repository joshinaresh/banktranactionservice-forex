﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public enum Currency
    {
        INR = 1,
        USD = 2,
        THB = 3,
        AUD = 4,
        GBP = 5,
        CHF = 6

    }
}
